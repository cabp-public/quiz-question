import HttpService from './HttpService';

/**
 * Singleton implementation for the Quiz App
 * @param _options
 * @returns {App}
 * @constructor
 */
const App = function (_options) {
  'use strict';

  let _instance;

  /**
   * Quiz
   * @class
   */
  class App {
    /**
     * Constructor for the Quiz Class
     * @param options
     */
    constructor(options) {
      // DOM Elements
      this.dom = {};

      Object.keys(options.domSelectors).forEach(groupKey => {
        const elGroup = options.domSelectors[groupKey];
        this.dom[groupKey] = {};

        Object.keys(elGroup).forEach(key => {
          const byId = Boolean(options.domSelectors[groupKey][key].id);
          const selector = options.domSelectors[groupKey][key].selector;

          this.dom[groupKey][key] = byId
            ? document.getElementById(selector)
            : document.querySelector(selector);
        });
      });

      // HTTP Service
      this.http = HttpService(options.http);

      // Question
      this.data = {};

      // Bindings
      this.toggleElementVisible = this.toggleElementVisible.bind(this);
      this.hideElements = this.hideElements.bind(this);
      this.resetOptions = this.resetOptions.bind(this);
      this.onOptionClick = this.onOptionClick.bind(this);
      this.onDataLoad = this.onDataLoad.bind(this);
      this.submitAnswer = this.submitAnswer.bind(this);
      this.setup = this.setup.bind(this);
      this.start = this.start.bind(this);
    }

    // Hide ONE element
    toggleElementVisible(key) {
      const hideClassName = 'display-none';
      const el = this.dom[key];
      const classValues = el.className.split(' ');
      const defaultValues = el.className.split(hideClassName).join('');

      if (false === classValues.includes(hideClassName)) {
        classValues.push('display-none');
        this.dom[key].className = classValues.join(' ');
      }
      else {
        this.dom[key].className = defaultValues;
      }
    }

    // Hide ALL elements except the given ones
    hideElements(groupKeys, exceptKeys = []) {
      const hideClassName = 'display-none';

      groupKeys.forEach(groupKey => {
        const group = this.dom[groupKey];

        Object.keys(group).forEach(key => {
          const el = group[key];
          const classValues = el.className.split(' ');
          const defaultValues = el.className.split(hideClassName);

          if (false === exceptKeys.includes(key)) {
            if (false === classValues.includes(hideClassName)) {
              classValues.push('display-none');
            }

            el.className = classValues.join(' ');
          }
          else {
            el.className = defaultValues;
          }
        });
      });
    }

    resetOptions() {
      const keys = Object.keys(this.dom.rest.options);

      keys.forEach(key => {
        this.dom.rest.options[key].className = '';
      });
    }

    onOptionClick(event) {
      const el = event.target;
      const selectedIndex = Number(el.getAttribute('data-value'));

      this.resetOptions();
      this.dom.rest.options[selectedIndex].className = 'user-answer';
      this.data.selection = selectedIndex;
      this.dom.rest.submit.removeAttribute('disabled');
    }

    submitAnswer(event) {
      const { answer, selection } = this.data;
      const assert = selection === answer;
      const classTrue = 'correct-answer';
      const classFalse = 'wrong-answer';
      const elAnswer = this.dom.rest.options[answer];
      const elSelection = this.dom.rest.options[selection];

      elAnswer.className = classTrue;
      elSelection.className = assert ? classTrue : classFalse;

      console.log('answer', answer);
      console.log('selection', selection);
      console.log('assert', assert);
    }

    onDataLoad(results) {
      const { data } = results;
      const elQuestion = this.dom.rest.question;
      const elList = this.dom.rest.list;

      this.data = data;
      this.dom.rest.options = [];

      // 1. Hide ALL elements, except content
      this.hideElements(['parents'], ['content']);

      // 2. Display question and options
      elQuestion.innerHTML = data['question'];

      data['options'].forEach((option, index) => {
        const el = document.createElement('div');

        el.innerHTML = option;
        el.setAttribute('data-value', index);
        el.addEventListener('click', this.onOptionClick);

        elList.appendChild(el);
        this.dom.rest.options[index] = el;
      });
    }

    setup() {
      // 1. Hide elements, except instructions and start button
      this.hideElements(['parents'], ['instructions']);

      // 2. Event Listeners
      this.dom.rest.startButton.addEventListener('click', this.start);
      this.dom.rest.submit.addEventListener('click', this.submitAnswer);

      // 3. Disable submit
      this.dom.rest.submit.setAttribute('disabled', true);
    }

    /**
     * Start the Quiz experience
     */
    start(event) {
      const resourceId = this.dom.rest.resourceId.value;
      const endpoint = `/questions/${resourceId}`;

      // Hide ALL elements, except loader
      this.hideElements(['parents'], ['loader']);

      // Fetch data from jsonMock API
      this.http.get('jsonMock', endpoint, this.onDataLoad);
    }

    /**
     * Get an instance of the HTTP Service
     * @returns {*}
     */
    static getInstance() {
      if (!_instance) {
        _instance = new this(_options);
      }

      return _instance;
    }
  }

  return App.getInstance();
};

export default App;

