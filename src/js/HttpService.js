/**
 * Singleton implementation for the HTTP Service
 * @param _options
 * @returns {HttpService}
 * @constructor
 */
const HttpService = function (_options) {
  'use strict';

  let _instance;

  /**
   * HTTP Service
   * @class
   */
  class HttpService {
    /**
     * Constructor for the HTTP Service
     * @param options
     */
    constructor(options) {
      const { api } = options;
      this._baseUrl = {};
      this._headers = options['headers'];

      Object.keys(api).forEach(key => {
        this._baseUrl[key] = api[key]['protocol'] + '://' + api[key]['host'] + api[key]['basePath'];
      });

      // Bindings
      this.execFetch = this.execFetch.bind(this);
      this.get = this.get.bind(this);
      this.post = this.post.bind(this);
    }

    /**
     * Fetch from URL
     * @param url
     * @param options
     * @param callback
     */
    execFetch(url, options, callback) {
      fetch(url, options)
        .then((response) => {
          if (!response.ok) {
            throw Error(response.statusText);
          }

          return response.json();
        })
        .then((json) => {
          callback !== null && callback(json);
        })
        .catch((error) => {
          console.error(error);
        });
    }

    /**
     * Perform a GET request
     * @param key
     * @param endpoint
     * @param callback
     * @param headers
     */
    get(key, endpoint, callback, headers) {
      const url = this._baseUrl[key] + endpoint;
      const options = {
        method: 'GET',
        headers: headers || this._headers
      };

      try {
        this.execFetch(url, options, callback || null);
      }
      catch (e) {
        console.error(e);
      }
    }

    /**
     * Perform a POST request
     * @param key
     * @param endpoint
     * @param data
     * @param callback
     */
    post(key, endpoint, data, callback) {
      const url = this._baseUrl[key] + endpoint;

      const options = {
        method: 'POST',
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json'
        }
      };

      try {
        this.execFetch(url, options, callback || null);
      }
      catch (e) {
        console.error(e);
      }
    }

    put() {
    }

    delete() {
    }

    /**
     * Get an instance of the HTTP Service
     * @returns {*}
     */
    static getInstance() {
      if (!_instance) {
        _instance = new this(_options);
      }

      return _instance;
    }
  }

  return HttpService.getInstance();
};

export default HttpService;
