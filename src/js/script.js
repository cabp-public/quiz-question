import App from './QuizApp';

const AppQuiz = App({
  domSelectors: {
    parents: {
      instructions: { id: false, selector: 'div.instructions-wrapper' },
      loader: { id: true, selector: 'loader-view' },
      content: { id: true, selector: 'quiz' }
    },
    rest: {
      startButton: { id: true, selector: 'start-button' },
      resourceId: { id: true, selector: 'current-question-id' },
      question: { id: true, selector: 'question' },
      list: { id: false, selector: 'div.list-group' },
      submit: { id: true, selector: 'submit-button' },
      options: []
    }
  },
  http: {
    api: {
      jsonMock: {
        protocol: 'https',
        host: 'jsonmock.hackerrank.com',
        basePath: '/api'
      }
    },
    headers: {
      'Content-Type': 'application/json'
    }
  }
});

AppQuiz.setup();

